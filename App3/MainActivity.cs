﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;

namespace App3
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, IOnMapReadyCallback
    {
        public void OnMapReady(GoogleMap map)
        {
            MarkerOptions markerOpt1 = new MarkerOptions();
            markerOpt1.SetPosition(new LatLng(41.4036, 2.1744));
            markerOpt1.SetTitle("Customer 1");
        
            map.AddMarker(markerOpt1);

            MarkerOptions markerOpt2 = new MarkerOptions();
            markerOpt2.SetPosition(new LatLng(41.3640, 2.1675));
            markerOpt2.SetTitle("Customer 2");

            map.AddMarker(markerOpt2);

            MarkerOptions markerOpt3 = new MarkerOptions();
            markerOpt3.SetPosition(new LatLng(41.3796, 2.1240));
            markerOpt3.SetTitle("Customer 3");

            map.AddMarker(markerOpt3);
        
            // origin & destination -> MRW headquarters
            LatLng originANDdestination = new LatLng(41.3510622,2.118011);

            MarkerOptions markerOptOrigin= new MarkerOptions();
            markerOptOrigin.SetPosition(originANDdestination);
            markerOptOrigin.SetTitle("Origin");

            var bmDescriptor = BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueCyan);
            markerOptOrigin.SetIcon(bmDescriptor);

            map.AddMarker(markerOptOrigin);

            // Barcelona
            LatLng location = new LatLng(41.3851, 2.1734);

            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(location);
            builder.Zoom(12);

            CameraPosition cameraPosition = builder.Build();

            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);

            map.MoveCamera(cameraUpdate);

        // GET request
        //https://maps.googleapis.com/maps/api/directions/json?
        //    origin = 41.3525905,2.1157474 & destination = 41.3525905,2.1157474
        //          & waypoints = optimize:true | via:41.4036,2.1744 | via:41.3640,2.1675 | via:41.3796,2.1240
        //                   & key = MYAPP_KEY

        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.mapLayout);

            var mapFragment = (MapFragment)FragmentManager.FindFragmentById(Resource.Id.map);
            mapFragment.GetMapAsync(this);
        }
    }
}